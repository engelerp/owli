EESchema Schematic File Version 4
LIBS:Untitled-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB5705E
P 4500 1350
AR Path="/5FB5705E" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB5705E" Ref="S1"  Part="1" 
AR Path="/5FB9A349/5FB5705E" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB5705E" Ref="S6"  Part="1" 
AR Path="/5FC17D06/5FB5705E" Ref="S11"  Part="1" 
AR Path="/5FC180F4/5FB5705E" Ref="S16"  Part="1" 
AR Path="/5FC18879/5FB5705E" Ref="S21"  Part="1" 
AR Path="/5FC18B2A/5FB5705E" Ref="S26"  Part="1" 
AR Path="/5FC18E87/5FB5705E" Ref="S31"  Part="1" 
AR Path="/5FC1911F/5FB5705E" Ref="S36"  Part="1" 
AR Path="/5FC193F7/5FB5705E" Ref="S41"  Part="1" 
AR Path="/5FC19686/5FB5705E" Ref="S46"  Part="1" 
F 0 "S46" V 4900 800 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 450 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 1350 50  0001 C CNN
F 3 "" H 4500 1350 50  0001 C CNN
	1    4500 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 1100 4300 850 
Wire Wire Line
	3650 850  3650 1100
Text Label 4300 850  0    50   ~ 0
MOSD_1
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB644C5
P 4500 2800
AR Path="/5FB644C5" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB644C5" Ref="S2"  Part="1" 
AR Path="/5FB9A349/5FB644C5" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB644C5" Ref="S7"  Part="1" 
AR Path="/5FC17D06/5FB644C5" Ref="S12"  Part="1" 
AR Path="/5FC180F4/5FB644C5" Ref="S17"  Part="1" 
AR Path="/5FC18879/5FB644C5" Ref="S22"  Part="1" 
AR Path="/5FC18B2A/5FB644C5" Ref="S27"  Part="1" 
AR Path="/5FC18E87/5FB644C5" Ref="S32"  Part="1" 
AR Path="/5FC1911F/5FB644C5" Ref="S37"  Part="1" 
AR Path="/5FC193F7/5FB644C5" Ref="S42"  Part="1" 
AR Path="/5FC19686/5FB644C5" Ref="S47"  Part="1" 
F 0 "S47" V 4900 2250 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 1900 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 2550 4300 2300
Wire Wire Line
	3650 2300 3650 2550
Text Label 4300 2300 0    50   ~ 0
MOSD_2
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6CC1B
P 4500 4250
AR Path="/5FB6CC1B" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC1B" Ref="S3"  Part="1" 
AR Path="/5FB9A349/5FB6CC1B" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC1B" Ref="S8"  Part="1" 
AR Path="/5FC17D06/5FB6CC1B" Ref="S13"  Part="1" 
AR Path="/5FC180F4/5FB6CC1B" Ref="S18"  Part="1" 
AR Path="/5FC18879/5FB6CC1B" Ref="S23"  Part="1" 
AR Path="/5FC18B2A/5FB6CC1B" Ref="S28"  Part="1" 
AR Path="/5FC18E87/5FB6CC1B" Ref="S33"  Part="1" 
AR Path="/5FC1911F/5FB6CC1B" Ref="S38"  Part="1" 
AR Path="/5FC193F7/5FB6CC1B" Ref="S43"  Part="1" 
AR Path="/5FC19686/5FB6CC1B" Ref="S48"  Part="1" 
F 0 "S48" V 4900 3700 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 3350 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 4250 50  0001 C CNN
F 3 "" H 4500 4250 50  0001 C CNN
	1    4500 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 4000 4300 3750
Wire Wire Line
	3650 3750 3650 4000
Text Label 4300 3750 0    50   ~ 0
MOSD_3
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6CC33
P 4500 5700
AR Path="/5FB6CC33" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC33" Ref="S4"  Part="1" 
AR Path="/5FB9A349/5FB6CC33" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC33" Ref="S9"  Part="1" 
AR Path="/5FC17D06/5FB6CC33" Ref="S14"  Part="1" 
AR Path="/5FC180F4/5FB6CC33" Ref="S19"  Part="1" 
AR Path="/5FC18879/5FB6CC33" Ref="S24"  Part="1" 
AR Path="/5FC18B2A/5FB6CC33" Ref="S29"  Part="1" 
AR Path="/5FC18E87/5FB6CC33" Ref="S34"  Part="1" 
AR Path="/5FC1911F/5FB6CC33" Ref="S39"  Part="1" 
AR Path="/5FC193F7/5FB6CC33" Ref="S44"  Part="1" 
AR Path="/5FC19686/5FB6CC33" Ref="S49"  Part="1" 
F 0 "S49" V 4900 5150 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 4800 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 5700 50  0001 C CNN
F 3 "" H 4500 5700 50  0001 C CNN
	1    4500 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 5450 4300 5200
Wire Wire Line
	3650 5200 3650 5450
Text Label 4300 5200 0    50   ~ 0
MOSD_4
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6EB35
P 4550 7150
AR Path="/5FB6EB35" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6EB35" Ref="S5"  Part="1" 
AR Path="/5FB9A349/5FB6EB35" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6EB35" Ref="S10"  Part="1" 
AR Path="/5FC17D06/5FB6EB35" Ref="S15"  Part="1" 
AR Path="/5FC180F4/5FB6EB35" Ref="S20"  Part="1" 
AR Path="/5FC18879/5FB6EB35" Ref="S25"  Part="1" 
AR Path="/5FC18B2A/5FB6EB35" Ref="S30"  Part="1" 
AR Path="/5FC18E87/5FB6EB35" Ref="S35"  Part="1" 
AR Path="/5FC1911F/5FB6EB35" Ref="S40"  Part="1" 
AR Path="/5FC193F7/5FB6EB35" Ref="S45"  Part="1" 
AR Path="/5FC19686/5FB6EB35" Ref="S50"  Part="1" 
F 0 "S50" V 4950 6600 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5050 6250 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4550 7150 50  0001 C CNN
F 3 "" H 4550 7150 50  0001 C CNN
	1    4550 7150
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 6900 4350 6650
Wire Wire Line
	3700 6650 3700 6900
Text Label 4350 6650 0    50   ~ 0
MOSD_5
Text HLabel 3700 7400 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 5950 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 4500 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 3050 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 1600 0    50   BiDi ~ 0
N_NO
Text HLabel 4300 1600 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 3050 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 4500 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 5950 2    50   BiDi ~ 0
P_NO
Text HLabel 4350 7400 2    50   BiDi ~ 0
P_NO
Text HLabel 3650 850  0    50   Input ~ 0
5V_IN
Text HLabel 3650 3750 0    50   Input ~ 0
5V_IN
Text HLabel 3650 5200 0    50   Input ~ 0
5V_IN
Text HLabel 3700 6650 0    50   Input ~ 0
5V_IN
Text HLabel 3650 2300 0    50   Input ~ 0
5V_IN
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8FECA
P 6050 1150
AR Path="/5FB8FECA" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FECA" Ref="M1"  Part="1" 
AR Path="/5FB9A349/5FB8FECA" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FECA" Ref="M6"  Part="1" 
AR Path="/5FC17D06/5FB8FECA" Ref="M11"  Part="1" 
AR Path="/5FC180F4/5FB8FECA" Ref="M16"  Part="1" 
AR Path="/5FC18879/5FB8FECA" Ref="M21"  Part="1" 
AR Path="/5FC18B2A/5FB8FECA" Ref="M26"  Part="1" 
AR Path="/5FC18E87/5FB8FECA" Ref="M31"  Part="1" 
AR Path="/5FC1911F/5FB8FECA" Ref="M36"  Part="1" 
AR Path="/5FC193F7/5FB8FECA" Ref="M41"  Part="1" 
AR Path="/5FC19686/5FB8FECA" Ref="M46"  Part="1" 
F 0 "M46" H 6208 946 50  0000 L CNN
F 1 "NTD3055L170" H 6208 855 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 1150 50  0001 C CNN
F 3 "" H 6050 1150 50  0001 C CNN
	1    6050 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8FED0
P 5700 1400
AR Path="/5FB8FED0" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FED0" Ref="R1"  Part="1" 
AR Path="/5FB9A349/5FB8FED0" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FED0" Ref="R16"  Part="1" 
AR Path="/5FC17D06/5FB8FED0" Ref="R31"  Part="1" 
AR Path="/5FC180F4/5FB8FED0" Ref="R46"  Part="1" 
AR Path="/5FC18879/5FB8FED0" Ref="R61"  Part="1" 
AR Path="/5FC18B2A/5FB8FED0" Ref="R76"  Part="1" 
AR Path="/5FC18E87/5FB8FED0" Ref="R91"  Part="1" 
AR Path="/5FC1911F/5FB8FED0" Ref="R106"  Part="1" 
AR Path="/5FC193F7/5FB8FED0" Ref="R121"  Part="1" 
AR Path="/5FC19686/5FB8FED0" Ref="R136"  Part="1" 
F 0 "R136" V 5493 1400 50  0000 C CNN
F 1 "1.2k" V 5584 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 1400 50  0001 C CNN
F 3 "~" H 5700 1400 50  0001 C CNN
	1    5700 1400
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8FED6
P 5850 1550
AR Path="/5FB8FED6" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FED6" Ref="R6"  Part="1" 
AR Path="/5FB9A349/5FB8FED6" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FED6" Ref="R21"  Part="1" 
AR Path="/5FC17D06/5FB8FED6" Ref="R36"  Part="1" 
AR Path="/5FC180F4/5FB8FED6" Ref="R51"  Part="1" 
AR Path="/5FC18879/5FB8FED6" Ref="R66"  Part="1" 
AR Path="/5FC18B2A/5FB8FED6" Ref="R81"  Part="1" 
AR Path="/5FC18E87/5FB8FED6" Ref="R96"  Part="1" 
AR Path="/5FC1911F/5FB8FED6" Ref="R111"  Part="1" 
AR Path="/5FC193F7/5FB8FED6" Ref="R126"  Part="1" 
AR Path="/5FC19686/5FB8FED6" Ref="R141"  Part="1" 
F 0 "R141" H 5650 1600 50  0000 L CNN
F 1 "43k" H 5650 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 1550 50  0001 C CNN
F 3 "~" H 5850 1550 50  0001 C CNN
	1    5850 1550
	1    0    0    -1  
$EndComp
Text Label 6150 1200 0    50   ~ 0
MOSD_1
Connection ~ 5850 1400
Text HLabel 6150 1600 3    50   Input ~ 0
GND_IN
Text HLabel 5850 1700 3    50   Input ~ 0
GND_IN
Text HLabel 5550 1400 0    50   BiDi ~ 0
CE_1
Text HLabel 5550 7000 0    50   BiDi ~ 0
CE_5
Text HLabel 5550 5600 0    50   BiDi ~ 0
CE_4
Text HLabel 5550 4200 0    50   BiDi ~ 0
CE_3
Text HLabel 5550 2800 0    50   BiDi ~ 0
CE_2
Text HLabel 6150 7200 3    50   Input ~ 0
GND_IN
Text HLabel 5850 7300 3    50   Input ~ 0
GND_IN
Text HLabel 5850 5900 3    50   Input ~ 0
GND_IN
Text HLabel 6150 5800 3    50   Input ~ 0
GND_IN
Text HLabel 6150 4400 3    50   Input ~ 0
GND_IN
Text HLabel 5850 4500 3    50   Input ~ 0
GND_IN
Text HLabel 5850 3100 3    50   Input ~ 0
GND_IN
Text HLabel 6150 3000 3    50   Input ~ 0
GND_IN
Text Label 6150 2600 0    50   ~ 0
MOSD_2
$Comp
L Device:R R?
U 1 1 5FB8E50C
P 5850 2950
AR Path="/5FB8E50C" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E50C" Ref="R7"  Part="1" 
AR Path="/5FB9A349/5FB8E50C" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E50C" Ref="R22"  Part="1" 
AR Path="/5FC17D06/5FB8E50C" Ref="R37"  Part="1" 
AR Path="/5FC180F4/5FB8E50C" Ref="R52"  Part="1" 
AR Path="/5FC18879/5FB8E50C" Ref="R67"  Part="1" 
AR Path="/5FC18B2A/5FB8E50C" Ref="R82"  Part="1" 
AR Path="/5FC18E87/5FB8E50C" Ref="R97"  Part="1" 
AR Path="/5FC1911F/5FB8E50C" Ref="R112"  Part="1" 
AR Path="/5FC193F7/5FB8E50C" Ref="R127"  Part="1" 
AR Path="/5FC19686/5FB8E50C" Ref="R142"  Part="1" 
F 0 "R142" H 5650 3000 50  0000 L CNN
F 1 "43k" H 5650 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8E506
P 5700 2800
AR Path="/5FB8E506" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E506" Ref="R2"  Part="1" 
AR Path="/5FB9A349/5FB8E506" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E506" Ref="R17"  Part="1" 
AR Path="/5FC17D06/5FB8E506" Ref="R32"  Part="1" 
AR Path="/5FC180F4/5FB8E506" Ref="R47"  Part="1" 
AR Path="/5FC18879/5FB8E506" Ref="R62"  Part="1" 
AR Path="/5FC18B2A/5FB8E506" Ref="R77"  Part="1" 
AR Path="/5FC18E87/5FB8E506" Ref="R92"  Part="1" 
AR Path="/5FC1911F/5FB8E506" Ref="R107"  Part="1" 
AR Path="/5FC193F7/5FB8E506" Ref="R122"  Part="1" 
AR Path="/5FC19686/5FB8E506" Ref="R137"  Part="1" 
F 0 "R137" V 5493 2800 50  0000 C CNN
F 1 "1.2k" V 5584 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 2800 50  0001 C CNN
F 3 "~" H 5700 2800 50  0001 C CNN
	1    5700 2800
	0    1    1    0   
$EndComp
Connection ~ 5850 2800
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8E500
P 6050 2550
AR Path="/5FB8E500" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E500" Ref="M2"  Part="1" 
AR Path="/5FB9A349/5FB8E500" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E500" Ref="M7"  Part="1" 
AR Path="/5FC17D06/5FB8E500" Ref="M12"  Part="1" 
AR Path="/5FC180F4/5FB8E500" Ref="M17"  Part="1" 
AR Path="/5FC18879/5FB8E500" Ref="M22"  Part="1" 
AR Path="/5FC18B2A/5FB8E500" Ref="M27"  Part="1" 
AR Path="/5FC18E87/5FB8E500" Ref="M32"  Part="1" 
AR Path="/5FC1911F/5FB8E500" Ref="M37"  Part="1" 
AR Path="/5FC193F7/5FB8E500" Ref="M42"  Part="1" 
AR Path="/5FC19686/5FB8E500" Ref="M47"  Part="1" 
F 0 "M47" H 6208 2346 50  0000 L CNN
F 1 "NTD3055L170" H 6208 2255 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 2550 50  0001 C CNN
F 3 "" H 6050 2550 50  0001 C CNN
	1    6050 2550
	1    0    0    -1  
$EndComp
Text Label 6150 4000 0    50   ~ 0
MOSD_3
$Comp
L Device:R R?
U 1 1 5FB8C70A
P 5850 4350
AR Path="/5FB8C70A" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C70A" Ref="R8"  Part="1" 
AR Path="/5FB9A349/5FB8C70A" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C70A" Ref="R23"  Part="1" 
AR Path="/5FC17D06/5FB8C70A" Ref="R38"  Part="1" 
AR Path="/5FC180F4/5FB8C70A" Ref="R53"  Part="1" 
AR Path="/5FC18879/5FB8C70A" Ref="R68"  Part="1" 
AR Path="/5FC18B2A/5FB8C70A" Ref="R83"  Part="1" 
AR Path="/5FC18E87/5FB8C70A" Ref="R98"  Part="1" 
AR Path="/5FC1911F/5FB8C70A" Ref="R113"  Part="1" 
AR Path="/5FC193F7/5FB8C70A" Ref="R128"  Part="1" 
AR Path="/5FC19686/5FB8C70A" Ref="R143"  Part="1" 
F 0 "R143" H 5650 4400 50  0000 L CNN
F 1 "43k" H 5650 4300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 4350 50  0001 C CNN
F 3 "~" H 5850 4350 50  0001 C CNN
	1    5850 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8C704
P 5700 4200
AR Path="/5FB8C704" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C704" Ref="R3"  Part="1" 
AR Path="/5FB9A349/5FB8C704" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C704" Ref="R18"  Part="1" 
AR Path="/5FC17D06/5FB8C704" Ref="R33"  Part="1" 
AR Path="/5FC180F4/5FB8C704" Ref="R48"  Part="1" 
AR Path="/5FC18879/5FB8C704" Ref="R63"  Part="1" 
AR Path="/5FC18B2A/5FB8C704" Ref="R78"  Part="1" 
AR Path="/5FC18E87/5FB8C704" Ref="R93"  Part="1" 
AR Path="/5FC1911F/5FB8C704" Ref="R108"  Part="1" 
AR Path="/5FC193F7/5FB8C704" Ref="R123"  Part="1" 
AR Path="/5FC19686/5FB8C704" Ref="R138"  Part="1" 
F 0 "R138" V 5493 4200 50  0000 C CNN
F 1 "1.2k" V 5584 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 4200 50  0001 C CNN
F 3 "~" H 5700 4200 50  0001 C CNN
	1    5700 4200
	0    1    1    0   
$EndComp
Connection ~ 5850 4200
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8C6FE
P 6050 3950
AR Path="/5FB8C6FE" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C6FE" Ref="M3"  Part="1" 
AR Path="/5FB9A349/5FB8C6FE" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C6FE" Ref="M8"  Part="1" 
AR Path="/5FC17D06/5FB8C6FE" Ref="M13"  Part="1" 
AR Path="/5FC180F4/5FB8C6FE" Ref="M18"  Part="1" 
AR Path="/5FC18879/5FB8C6FE" Ref="M23"  Part="1" 
AR Path="/5FC18B2A/5FB8C6FE" Ref="M28"  Part="1" 
AR Path="/5FC18E87/5FB8C6FE" Ref="M33"  Part="1" 
AR Path="/5FC1911F/5FB8C6FE" Ref="M38"  Part="1" 
AR Path="/5FC193F7/5FB8C6FE" Ref="M43"  Part="1" 
AR Path="/5FC19686/5FB8C6FE" Ref="M48"  Part="1" 
F 0 "M48" H 6208 3746 50  0000 L CNN
F 1 "NTD3055L170" H 6208 3655 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 3950 50  0001 C CNN
F 3 "" H 6050 3950 50  0001 C CNN
	1    6050 3950
	1    0    0    -1  
$EndComp
Text Label 6150 5400 0    50   ~ 0
MOSD_4
$Comp
L Device:R R?
U 1 1 5FB88A40
P 5850 5750
AR Path="/5FB88A40" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A40" Ref="R9"  Part="1" 
AR Path="/5FB9A349/5FB88A40" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A40" Ref="R24"  Part="1" 
AR Path="/5FC17D06/5FB88A40" Ref="R39"  Part="1" 
AR Path="/5FC180F4/5FB88A40" Ref="R54"  Part="1" 
AR Path="/5FC18879/5FB88A40" Ref="R69"  Part="1" 
AR Path="/5FC18B2A/5FB88A40" Ref="R84"  Part="1" 
AR Path="/5FC18E87/5FB88A40" Ref="R99"  Part="1" 
AR Path="/5FC1911F/5FB88A40" Ref="R114"  Part="1" 
AR Path="/5FC193F7/5FB88A40" Ref="R129"  Part="1" 
AR Path="/5FC19686/5FB88A40" Ref="R144"  Part="1" 
F 0 "R144" H 5650 5800 50  0000 L CNN
F 1 "43k" H 5650 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 5750 50  0001 C CNN
F 3 "~" H 5850 5750 50  0001 C CNN
	1    5850 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB88A3A
P 5700 5600
AR Path="/5FB88A3A" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A3A" Ref="R4"  Part="1" 
AR Path="/5FB9A349/5FB88A3A" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A3A" Ref="R19"  Part="1" 
AR Path="/5FC17D06/5FB88A3A" Ref="R34"  Part="1" 
AR Path="/5FC180F4/5FB88A3A" Ref="R49"  Part="1" 
AR Path="/5FC18879/5FB88A3A" Ref="R64"  Part="1" 
AR Path="/5FC18B2A/5FB88A3A" Ref="R79"  Part="1" 
AR Path="/5FC18E87/5FB88A3A" Ref="R94"  Part="1" 
AR Path="/5FC1911F/5FB88A3A" Ref="R109"  Part="1" 
AR Path="/5FC193F7/5FB88A3A" Ref="R124"  Part="1" 
AR Path="/5FC19686/5FB88A3A" Ref="R139"  Part="1" 
F 0 "R139" V 5493 5600 50  0000 C CNN
F 1 "1.2k" V 5584 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 5600 50  0001 C CNN
F 3 "~" H 5700 5600 50  0001 C CNN
	1    5700 5600
	0    1    1    0   
$EndComp
Connection ~ 5850 5600
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB88A34
P 6050 5350
AR Path="/5FB88A34" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A34" Ref="M4"  Part="1" 
AR Path="/5FB9A349/5FB88A34" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A34" Ref="M9"  Part="1" 
AR Path="/5FC17D06/5FB88A34" Ref="M14"  Part="1" 
AR Path="/5FC180F4/5FB88A34" Ref="M19"  Part="1" 
AR Path="/5FC18879/5FB88A34" Ref="M24"  Part="1" 
AR Path="/5FC18B2A/5FB88A34" Ref="M29"  Part="1" 
AR Path="/5FC18E87/5FB88A34" Ref="M34"  Part="1" 
AR Path="/5FC1911F/5FB88A34" Ref="M39"  Part="1" 
AR Path="/5FC193F7/5FB88A34" Ref="M44"  Part="1" 
AR Path="/5FC19686/5FB88A34" Ref="M49"  Part="1" 
F 0 "M49" H 6208 5146 50  0000 L CNN
F 1 "NTD3055L170" H 6208 5055 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 5350 50  0001 C CNN
F 3 "" H 6050 5350 50  0001 C CNN
	1    6050 5350
	1    0    0    -1  
$EndComp
Text Label 6150 6800 0    50   ~ 0
MOSD_5
$Comp
L Device:R R?
U 1 1 5FB84198
P 5850 7150
AR Path="/5FB84198" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB84198" Ref="R10"  Part="1" 
AR Path="/5FB9A349/5FB84198" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB84198" Ref="R25"  Part="1" 
AR Path="/5FC17D06/5FB84198" Ref="R40"  Part="1" 
AR Path="/5FC180F4/5FB84198" Ref="R55"  Part="1" 
AR Path="/5FC18879/5FB84198" Ref="R70"  Part="1" 
AR Path="/5FC18B2A/5FB84198" Ref="R85"  Part="1" 
AR Path="/5FC18E87/5FB84198" Ref="R100"  Part="1" 
AR Path="/5FC1911F/5FB84198" Ref="R115"  Part="1" 
AR Path="/5FC193F7/5FB84198" Ref="R130"  Part="1" 
AR Path="/5FC19686/5FB84198" Ref="R145"  Part="1" 
F 0 "R145" H 5650 7200 50  0000 L CNN
F 1 "43k" H 5650 7100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 7150 50  0001 C CNN
F 3 "~" H 5850 7150 50  0001 C CNN
	1    5850 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB84192
P 5700 7000
AR Path="/5FB84192" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB84192" Ref="R5"  Part="1" 
AR Path="/5FB9A349/5FB84192" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB84192" Ref="R20"  Part="1" 
AR Path="/5FC17D06/5FB84192" Ref="R35"  Part="1" 
AR Path="/5FC180F4/5FB84192" Ref="R50"  Part="1" 
AR Path="/5FC18879/5FB84192" Ref="R65"  Part="1" 
AR Path="/5FC18B2A/5FB84192" Ref="R80"  Part="1" 
AR Path="/5FC18E87/5FB84192" Ref="R95"  Part="1" 
AR Path="/5FC1911F/5FB84192" Ref="R110"  Part="1" 
AR Path="/5FC193F7/5FB84192" Ref="R125"  Part="1" 
AR Path="/5FC19686/5FB84192" Ref="R140"  Part="1" 
F 0 "R140" V 5493 7000 50  0000 C CNN
F 1 "1.2k" V 5584 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 7000 50  0001 C CNN
F 3 "~" H 5700 7000 50  0001 C CNN
	1    5700 7000
	0    1    1    0   
$EndComp
Connection ~ 5850 7000
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8418C
P 6050 6750
AR Path="/5FB8418C" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8418C" Ref="M5"  Part="1" 
AR Path="/5FB9A349/5FB8418C" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8418C" Ref="M10"  Part="1" 
AR Path="/5FC17D06/5FB8418C" Ref="M15"  Part="1" 
AR Path="/5FC180F4/5FB8418C" Ref="M20"  Part="1" 
AR Path="/5FC18879/5FB8418C" Ref="M25"  Part="1" 
AR Path="/5FC18B2A/5FB8418C" Ref="M30"  Part="1" 
AR Path="/5FC18E87/5FB8418C" Ref="M35"  Part="1" 
AR Path="/5FC1911F/5FB8418C" Ref="M40"  Part="1" 
AR Path="/5FC193F7/5FB8418C" Ref="M45"  Part="1" 
AR Path="/5FC19686/5FB8418C" Ref="M50"  Part="1" 
F 0 "M50" H 6208 6546 50  0000 L CNN
F 1 "NTD3055L170" H 6208 6455 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 6750 50  0001 C CNN
F 3 "" H 6050 6750 50  0001 C CNN
	1    6050 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 5FC1F2E0
P 8850 1400
AR Path="/5FB4E0CA/5FC1F2E0" Ref="D6"  Part="1" 
AR Path="/5FB9AAE0/5FC1F2E0" Ref="D16"  Part="1" 
AR Path="/5FC17D06/5FC1F2E0" Ref="D26"  Part="1" 
AR Path="/5FC180F4/5FC1F2E0" Ref="D36"  Part="1" 
AR Path="/5FC18879/5FC1F2E0" Ref="D46"  Part="1" 
AR Path="/5FC18B2A/5FC1F2E0" Ref="D56"  Part="1" 
AR Path="/5FC18E87/5FC1F2E0" Ref="D66"  Part="1" 
AR Path="/5FC1911F/5FC1F2E0" Ref="D76"  Part="1" 
AR Path="/5FC193F7/5FC1F2E0" Ref="D86"  Part="1" 
AR Path="/5FC19686/5FC1F2E0" Ref="D96"  Part="1" 
F 0 "D96" H 8843 1145 50  0000 C CNN
F 1 "LED" H 8843 1236 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 1400 50  0001 C CNN
F 3 "~" H 8850 1400 50  0001 C CNN
	1    8850 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D7
U 1 1 5FC21A14
P 8850 2000
AR Path="/5FB4E0CA/5FC21A14" Ref="D7"  Part="1" 
AR Path="/5FB9AAE0/5FC21A14" Ref="D17"  Part="1" 
AR Path="/5FC17D06/5FC21A14" Ref="D27"  Part="1" 
AR Path="/5FC180F4/5FC21A14" Ref="D37"  Part="1" 
AR Path="/5FC18879/5FC21A14" Ref="D47"  Part="1" 
AR Path="/5FC18B2A/5FC21A14" Ref="D57"  Part="1" 
AR Path="/5FC18E87/5FC21A14" Ref="D67"  Part="1" 
AR Path="/5FC1911F/5FC21A14" Ref="D77"  Part="1" 
AR Path="/5FC193F7/5FC21A14" Ref="D87"  Part="1" 
AR Path="/5FC19686/5FC21A14" Ref="D97"  Part="1" 
F 0 "D97" H 8843 1745 50  0000 C CNN
F 1 "LED" H 8843 1836 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 2000 50  0001 C CNN
F 3 "~" H 8850 2000 50  0001 C CNN
	1    8850 2000
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D8
U 1 1 5FC245D3
P 8850 2600
AR Path="/5FB4E0CA/5FC245D3" Ref="D8"  Part="1" 
AR Path="/5FB9AAE0/5FC245D3" Ref="D18"  Part="1" 
AR Path="/5FC17D06/5FC245D3" Ref="D28"  Part="1" 
AR Path="/5FC180F4/5FC245D3" Ref="D38"  Part="1" 
AR Path="/5FC18879/5FC245D3" Ref="D48"  Part="1" 
AR Path="/5FC18B2A/5FC245D3" Ref="D58"  Part="1" 
AR Path="/5FC18E87/5FC245D3" Ref="D68"  Part="1" 
AR Path="/5FC1911F/5FC245D3" Ref="D78"  Part="1" 
AR Path="/5FC193F7/5FC245D3" Ref="D88"  Part="1" 
AR Path="/5FC19686/5FC245D3" Ref="D98"  Part="1" 
F 0 "D98" H 8843 2345 50  0000 C CNN
F 1 "LED" H 8843 2436 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 2600 50  0001 C CNN
F 3 "~" H 8850 2600 50  0001 C CNN
	1    8850 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D9
U 1 1 5FC24F3A
P 8850 3200
AR Path="/5FB4E0CA/5FC24F3A" Ref="D9"  Part="1" 
AR Path="/5FB9AAE0/5FC24F3A" Ref="D19"  Part="1" 
AR Path="/5FC17D06/5FC24F3A" Ref="D29"  Part="1" 
AR Path="/5FC180F4/5FC24F3A" Ref="D39"  Part="1" 
AR Path="/5FC18879/5FC24F3A" Ref="D49"  Part="1" 
AR Path="/5FC18B2A/5FC24F3A" Ref="D59"  Part="1" 
AR Path="/5FC18E87/5FC24F3A" Ref="D69"  Part="1" 
AR Path="/5FC1911F/5FC24F3A" Ref="D79"  Part="1" 
AR Path="/5FC193F7/5FC24F3A" Ref="D89"  Part="1" 
AR Path="/5FC19686/5FC24F3A" Ref="D99"  Part="1" 
F 0 "D99" H 8843 2945 50  0000 C CNN
F 1 "LED" H 8843 3036 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 3200 50  0001 C CNN
F 3 "~" H 8850 3200 50  0001 C CNN
	1    8850 3200
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D10
U 1 1 5FC25A6C
P 8850 3850
AR Path="/5FB4E0CA/5FC25A6C" Ref="D10"  Part="1" 
AR Path="/5FB9AAE0/5FC25A6C" Ref="D20"  Part="1" 
AR Path="/5FC17D06/5FC25A6C" Ref="D30"  Part="1" 
AR Path="/5FC180F4/5FC25A6C" Ref="D40"  Part="1" 
AR Path="/5FC18879/5FC25A6C" Ref="D50"  Part="1" 
AR Path="/5FC18B2A/5FC25A6C" Ref="D60"  Part="1" 
AR Path="/5FC18E87/5FC25A6C" Ref="D70"  Part="1" 
AR Path="/5FC1911F/5FC25A6C" Ref="D80"  Part="1" 
AR Path="/5FC193F7/5FC25A6C" Ref="D90"  Part="1" 
AR Path="/5FC19686/5FC25A6C" Ref="D100"  Part="1" 
F 0 "D100" H 8843 3595 50  0000 C CNN
F 1 "LED" H 8843 3686 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 3850 50  0001 C CNN
F 3 "~" H 8850 3850 50  0001 C CNN
	1    8850 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5FC2719C
P 8550 1400
AR Path="/5FB4E0CA/5FC2719C" Ref="R11"  Part="1" 
AR Path="/5FB9AAE0/5FC2719C" Ref="R26"  Part="1" 
AR Path="/5FC17D06/5FC2719C" Ref="R41"  Part="1" 
AR Path="/5FC180F4/5FC2719C" Ref="R56"  Part="1" 
AR Path="/5FC18879/5FC2719C" Ref="R71"  Part="1" 
AR Path="/5FC18B2A/5FC2719C" Ref="R86"  Part="1" 
AR Path="/5FC18E87/5FC2719C" Ref="R101"  Part="1" 
AR Path="/5FC1911F/5FC2719C" Ref="R116"  Part="1" 
AR Path="/5FC193F7/5FC2719C" Ref="R131"  Part="1" 
AR Path="/5FC19686/5FC2719C" Ref="R146"  Part="1" 
F 0 "R146" V 8343 1400 50  0000 C CNN
F 1 "150" V 8434 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 1400 50  0001 C CNN
F 3 "~" H 8550 1400 50  0001 C CNN
	1    8550 1400
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5FC27FE8
P 8550 2000
AR Path="/5FB4E0CA/5FC27FE8" Ref="R12"  Part="1" 
AR Path="/5FB9AAE0/5FC27FE8" Ref="R27"  Part="1" 
AR Path="/5FC17D06/5FC27FE8" Ref="R42"  Part="1" 
AR Path="/5FC180F4/5FC27FE8" Ref="R57"  Part="1" 
AR Path="/5FC18879/5FC27FE8" Ref="R72"  Part="1" 
AR Path="/5FC18B2A/5FC27FE8" Ref="R87"  Part="1" 
AR Path="/5FC18E87/5FC27FE8" Ref="R102"  Part="1" 
AR Path="/5FC1911F/5FC27FE8" Ref="R117"  Part="1" 
AR Path="/5FC193F7/5FC27FE8" Ref="R132"  Part="1" 
AR Path="/5FC19686/5FC27FE8" Ref="R147"  Part="1" 
F 0 "R147" V 8343 2000 50  0000 C CNN
F 1 "150" V 8434 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 2000 50  0001 C CNN
F 3 "~" H 8550 2000 50  0001 C CNN
	1    8550 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5FC285A6
P 8550 2600
AR Path="/5FB4E0CA/5FC285A6" Ref="R13"  Part="1" 
AR Path="/5FB9AAE0/5FC285A6" Ref="R28"  Part="1" 
AR Path="/5FC17D06/5FC285A6" Ref="R43"  Part="1" 
AR Path="/5FC180F4/5FC285A6" Ref="R58"  Part="1" 
AR Path="/5FC18879/5FC285A6" Ref="R73"  Part="1" 
AR Path="/5FC18B2A/5FC285A6" Ref="R88"  Part="1" 
AR Path="/5FC18E87/5FC285A6" Ref="R103"  Part="1" 
AR Path="/5FC1911F/5FC285A6" Ref="R118"  Part="1" 
AR Path="/5FC193F7/5FC285A6" Ref="R133"  Part="1" 
AR Path="/5FC19686/5FC285A6" Ref="R148"  Part="1" 
F 0 "R148" V 8343 2600 50  0000 C CNN
F 1 "150" V 8434 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 2600 50  0001 C CNN
F 3 "~" H 8550 2600 50  0001 C CNN
	1    8550 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5FC28FD1
P 8550 3200
AR Path="/5FB4E0CA/5FC28FD1" Ref="R14"  Part="1" 
AR Path="/5FB9AAE0/5FC28FD1" Ref="R29"  Part="1" 
AR Path="/5FC17D06/5FC28FD1" Ref="R44"  Part="1" 
AR Path="/5FC180F4/5FC28FD1" Ref="R59"  Part="1" 
AR Path="/5FC18879/5FC28FD1" Ref="R74"  Part="1" 
AR Path="/5FC18B2A/5FC28FD1" Ref="R89"  Part="1" 
AR Path="/5FC18E87/5FC28FD1" Ref="R104"  Part="1" 
AR Path="/5FC1911F/5FC28FD1" Ref="R119"  Part="1" 
AR Path="/5FC193F7/5FC28FD1" Ref="R134"  Part="1" 
AR Path="/5FC19686/5FC28FD1" Ref="R149"  Part="1" 
F 0 "R149" V 8343 3200 50  0000 C CNN
F 1 "150" V 8434 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3200 50  0001 C CNN
F 3 "~" H 8550 3200 50  0001 C CNN
	1    8550 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5FC29607
P 8550 3850
AR Path="/5FB4E0CA/5FC29607" Ref="R15"  Part="1" 
AR Path="/5FB9AAE0/5FC29607" Ref="R30"  Part="1" 
AR Path="/5FC17D06/5FC29607" Ref="R45"  Part="1" 
AR Path="/5FC180F4/5FC29607" Ref="R60"  Part="1" 
AR Path="/5FC18879/5FC29607" Ref="R75"  Part="1" 
AR Path="/5FC18B2A/5FC29607" Ref="R90"  Part="1" 
AR Path="/5FC18E87/5FC29607" Ref="R105"  Part="1" 
AR Path="/5FC1911F/5FC29607" Ref="R120"  Part="1" 
AR Path="/5FC193F7/5FC29607" Ref="R135"  Part="1" 
AR Path="/5FC19686/5FC29607" Ref="R150"  Part="1" 
F 0 "R150" V 8343 3850 50  0000 C CNN
F 1 "150" V 8434 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3850 50  0001 C CNN
F 3 "~" H 8550 3850 50  0001 C CNN
	1    8550 3850
	0    1    1    0   
$EndComp
Text Label 8400 1400 2    50   ~ 0
5V_IN
Text Label 8400 2000 2    50   ~ 0
5V_IN
Text Label 8400 2600 2    50   ~ 0
5V_IN
Text Label 8400 3200 2    50   ~ 0
5V_IN
Text Label 8400 3850 2    50   ~ 0
5V_IN
Text Label 9000 1400 0    50   ~ 0
MOSD_1
Text Label 9000 2000 0    50   ~ 0
MOSD_2
Text Label 9000 2600 0    50   ~ 0
MOSD_3
Text Label 9000 3200 0    50   ~ 0
MOSD_4
Text Label 9000 3850 0    50   ~ 0
MOSD_5
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 5FC41AE1
P 8750 4500
AR Path="/5FB9AAE0/5FC41AE1" Ref="J15"  Part="1" 
AR Path="/5FB4E0CA/5FC41AE1" Ref="J10"  Part="1" 
AR Path="/5FC17D06/5FC41AE1" Ref="J20"  Part="1" 
AR Path="/5FC180F4/5FC41AE1" Ref="J25"  Part="1" 
AR Path="/5FC18879/5FC41AE1" Ref="J30"  Part="1" 
AR Path="/5FC18B2A/5FC41AE1" Ref="J35"  Part="1" 
AR Path="/5FC18E87/5FC41AE1" Ref="J40"  Part="1" 
AR Path="/5FC1911F/5FC41AE1" Ref="J45"  Part="1" 
AR Path="/5FC193F7/5FC41AE1" Ref="J50"  Part="1" 
AR Path="/5FC19686/5FC41AE1" Ref="J55"  Part="1" 
F 0 "J55" H 8830 4492 50  0000 L CNN
F 1 "Conn_01x02" H 8830 4401 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 4500 50  0001 C CNN
F 3 "~" H 8750 4500 50  0001 C CNN
	1    8750 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 5FC42918
P 8750 4850
AR Path="/5FB9AAE0/5FC42918" Ref="J16"  Part="1" 
AR Path="/5FB4E0CA/5FC42918" Ref="J11"  Part="1" 
AR Path="/5FC17D06/5FC42918" Ref="J21"  Part="1" 
AR Path="/5FC180F4/5FC42918" Ref="J26"  Part="1" 
AR Path="/5FC18879/5FC42918" Ref="J31"  Part="1" 
AR Path="/5FC18B2A/5FC42918" Ref="J36"  Part="1" 
AR Path="/5FC18E87/5FC42918" Ref="J41"  Part="1" 
AR Path="/5FC1911F/5FC42918" Ref="J46"  Part="1" 
AR Path="/5FC193F7/5FC42918" Ref="J51"  Part="1" 
AR Path="/5FC19686/5FC42918" Ref="J56"  Part="1" 
F 0 "J56" H 8830 4842 50  0000 L CNN
F 1 "Conn_01x02" H 8830 4751 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 4850 50  0001 C CNN
F 3 "~" H 8750 4850 50  0001 C CNN
	1    8750 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 5FC43074
P 8750 5200
AR Path="/5FB9AAE0/5FC43074" Ref="J17"  Part="1" 
AR Path="/5FB4E0CA/5FC43074" Ref="J12"  Part="1" 
AR Path="/5FC17D06/5FC43074" Ref="J22"  Part="1" 
AR Path="/5FC180F4/5FC43074" Ref="J27"  Part="1" 
AR Path="/5FC18879/5FC43074" Ref="J32"  Part="1" 
AR Path="/5FC18B2A/5FC43074" Ref="J37"  Part="1" 
AR Path="/5FC18E87/5FC43074" Ref="J42"  Part="1" 
AR Path="/5FC1911F/5FC43074" Ref="J47"  Part="1" 
AR Path="/5FC193F7/5FC43074" Ref="J52"  Part="1" 
AR Path="/5FC19686/5FC43074" Ref="J57"  Part="1" 
F 0 "J57" H 8830 5192 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5101 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5200 50  0001 C CNN
F 3 "~" H 8750 5200 50  0001 C CNN
	1    8750 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J18
U 1 1 5FC43479
P 8750 5550
AR Path="/5FB9AAE0/5FC43479" Ref="J18"  Part="1" 
AR Path="/5FB4E0CA/5FC43479" Ref="J13"  Part="1" 
AR Path="/5FC17D06/5FC43479" Ref="J23"  Part="1" 
AR Path="/5FC180F4/5FC43479" Ref="J28"  Part="1" 
AR Path="/5FC18879/5FC43479" Ref="J33"  Part="1" 
AR Path="/5FC18B2A/5FC43479" Ref="J38"  Part="1" 
AR Path="/5FC18E87/5FC43479" Ref="J43"  Part="1" 
AR Path="/5FC1911F/5FC43479" Ref="J48"  Part="1" 
AR Path="/5FC193F7/5FC43479" Ref="J53"  Part="1" 
AR Path="/5FC19686/5FC43479" Ref="J58"  Part="1" 
F 0 "J58" H 8830 5542 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5451 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5550 50  0001 C CNN
F 3 "~" H 8750 5550 50  0001 C CNN
	1    8750 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J19
U 1 1 5FC43A22
P 8750 5900
AR Path="/5FB9AAE0/5FC43A22" Ref="J19"  Part="1" 
AR Path="/5FB4E0CA/5FC43A22" Ref="J14"  Part="1" 
AR Path="/5FC17D06/5FC43A22" Ref="J24"  Part="1" 
AR Path="/5FC180F4/5FC43A22" Ref="J29"  Part="1" 
AR Path="/5FC18879/5FC43A22" Ref="J34"  Part="1" 
AR Path="/5FC18B2A/5FC43A22" Ref="J39"  Part="1" 
AR Path="/5FC18E87/5FC43A22" Ref="J44"  Part="1" 
AR Path="/5FC1911F/5FC43A22" Ref="J49"  Part="1" 
AR Path="/5FC193F7/5FC43A22" Ref="J54"  Part="1" 
AR Path="/5FC19686/5FC43A22" Ref="J59"  Part="1" 
F 0 "J59" H 8830 5892 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5801 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5900 50  0001 C CNN
F 3 "~" H 8750 5900 50  0001 C CNN
	1    8750 5900
	1    0    0    -1  
$EndComp
Text Label 4300 1500 0    50   ~ 0
PPIV1
Text Label 3650 1500 2    50   ~ 0
NPIV1
Text Label 4300 2950 0    50   ~ 0
PPIV2
Text Label 4300 4400 0    50   ~ 0
PPIV3
Text Label 4300 5850 0    50   ~ 0
PPIV4
Text Label 4350 7300 0    50   ~ 0
PPIV5
Text Label 3650 2950 2    50   ~ 0
NPIV2
Text Label 3650 4400 2    50   ~ 0
NPIV3
Text Label 3650 5850 2    50   ~ 0
NPIV4
Text Label 3700 7300 2    50   ~ 0
NPIV5
Text Label 8550 4500 2    50   ~ 0
PPIV1
Text Label 8550 4600 2    50   ~ 0
NPIV1
Text Label 8550 4850 2    50   ~ 0
PPIV2
Text Label 8550 4950 2    50   ~ 0
NPIV2
Text Label 8550 5200 2    50   ~ 0
PPIV3
Text Label 8550 5300 2    50   ~ 0
NPIV3
Text Label 8550 5550 2    50   ~ 0
PPIV4
Text Label 8550 5650 2    50   ~ 0
NPIV4
Text Label 8550 5900 2    50   ~ 0
PPIV5
Text Label 8550 6000 2    50   ~ 0
NPIV5
Wire Wire Line
	4100 2300 4300 2300
Wire Wire Line
	3650 2300 3800 2300
$Comp
L Device:D D?
U 1 1 5FB6EB3B
P 4000 6650
AR Path="/5FB6EB3B" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6EB3B" Ref="D5"  Part="1" 
AR Path="/5FB9A349/5FB6EB3B" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6EB3B" Ref="D15"  Part="1" 
AR Path="/5FC17D06/5FB6EB3B" Ref="D25"  Part="1" 
AR Path="/5FC180F4/5FB6EB3B" Ref="D35"  Part="1" 
AR Path="/5FC18879/5FB6EB3B" Ref="D45"  Part="1" 
AR Path="/5FC18B2A/5FB6EB3B" Ref="D55"  Part="1" 
AR Path="/5FC18E87/5FB6EB3B" Ref="D65"  Part="1" 
AR Path="/5FC1911F/5FB6EB3B" Ref="D75"  Part="1" 
AR Path="/5FC193F7/5FB6EB3B" Ref="D85"  Part="1" 
AR Path="/5FC19686/5FB6EB3B" Ref="D95"  Part="1" 
F 0 "D95" V 3900 6750 50  0000 L CNN
F 1 "DIODE" V 4000 6700 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4000 6650 50  0001 C CNN
F 3 "~" H 4000 6650 50  0001 C CNN
	1    4000 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FB6CC39
P 3950 5200
AR Path="/5FB6CC39" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC39" Ref="D4"  Part="1" 
AR Path="/5FB9A349/5FB6CC39" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC39" Ref="D14"  Part="1" 
AR Path="/5FC17D06/5FB6CC39" Ref="D24"  Part="1" 
AR Path="/5FC180F4/5FB6CC39" Ref="D34"  Part="1" 
AR Path="/5FC18879/5FB6CC39" Ref="D44"  Part="1" 
AR Path="/5FC18B2A/5FB6CC39" Ref="D54"  Part="1" 
AR Path="/5FC18E87/5FB6CC39" Ref="D64"  Part="1" 
AR Path="/5FC1911F/5FB6CC39" Ref="D74"  Part="1" 
AR Path="/5FC193F7/5FB6CC39" Ref="D84"  Part="1" 
AR Path="/5FC19686/5FB6CC39" Ref="D94"  Part="1" 
F 0 "D94" V 3850 5300 50  0000 L CNN
F 1 "DIODE" V 3950 5250 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 5200 50  0001 C CNN
F 3 "~" H 3950 5200 50  0001 C CNN
	1    3950 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FB6CC21
P 3950 3750
AR Path="/5FB6CC21" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC21" Ref="D3"  Part="1" 
AR Path="/5FB9A349/5FB6CC21" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC21" Ref="D13"  Part="1" 
AR Path="/5FC17D06/5FB6CC21" Ref="D23"  Part="1" 
AR Path="/5FC180F4/5FB6CC21" Ref="D33"  Part="1" 
AR Path="/5FC18879/5FB6CC21" Ref="D43"  Part="1" 
AR Path="/5FC18B2A/5FB6CC21" Ref="D53"  Part="1" 
AR Path="/5FC18E87/5FB6CC21" Ref="D63"  Part="1" 
AR Path="/5FC1911F/5FB6CC21" Ref="D73"  Part="1" 
AR Path="/5FC193F7/5FB6CC21" Ref="D83"  Part="1" 
AR Path="/5FC19686/5FB6CC21" Ref="D93"  Part="1" 
F 0 "D93" V 3850 3850 50  0000 L CNN
F 1 "DIODE" V 3950 3800 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 3750 50  0001 C CNN
F 3 "~" H 3950 3750 50  0001 C CNN
	1    3950 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FB57070
P 3950 850
AR Path="/5FB57070" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB57070" Ref="D1"  Part="1" 
AR Path="/5FB9A349/5FB57070" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB57070" Ref="D11"  Part="1" 
AR Path="/5FC17D06/5FB57070" Ref="D21"  Part="1" 
AR Path="/5FC180F4/5FB57070" Ref="D31"  Part="1" 
AR Path="/5FC18879/5FB57070" Ref="D41"  Part="1" 
AR Path="/5FC18B2A/5FB57070" Ref="D51"  Part="1" 
AR Path="/5FC18E87/5FB57070" Ref="D61"  Part="1" 
AR Path="/5FC1911F/5FB57070" Ref="D71"  Part="1" 
AR Path="/5FC193F7/5FB57070" Ref="D81"  Part="1" 
AR Path="/5FC19686/5FB57070" Ref="D91"  Part="1" 
F 0 "D91" V 3850 950 50  0000 L CNN
F 1 "DIODE" V 3950 900 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 850 50  0001 C CNN
F 3 "~" H 3950 850 50  0001 C CNN
	1    3950 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FB644CB
P 3950 2300
AR Path="/5FB644CB" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB644CB" Ref="D2"  Part="1" 
AR Path="/5FB9A349/5FB644CB" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB644CB" Ref="D12"  Part="1" 
AR Path="/5FC17D06/5FB644CB" Ref="D22"  Part="1" 
AR Path="/5FC180F4/5FB644CB" Ref="D32"  Part="1" 
AR Path="/5FC18879/5FB644CB" Ref="D42"  Part="1" 
AR Path="/5FC18B2A/5FB644CB" Ref="D52"  Part="1" 
AR Path="/5FC18E87/5FB644CB" Ref="D62"  Part="1" 
AR Path="/5FC1911F/5FB644CB" Ref="D72"  Part="1" 
AR Path="/5FC193F7/5FB644CB" Ref="D82"  Part="1" 
AR Path="/5FC19686/5FB644CB" Ref="D92"  Part="1" 
F 0 "D92" V 3850 2400 50  0000 L CNN
F 1 "DIODE" V 3950 2350 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 2300 50  0001 C CNN
F 3 "~" H 3950 2300 50  0001 C CNN
	1    3950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3750 4300 3750
Wire Wire Line
	3650 3750 3800 3750
Wire Wire Line
	3650 5200 3800 5200
Wire Wire Line
	4100 5200 4300 5200
Wire Wire Line
	3700 6650 3850 6650
Wire Wire Line
	4150 6650 4350 6650
Wire Wire Line
	3650 850  3800 850 
Wire Wire Line
	4100 850  4300 850 
$EndSCHEMATC
