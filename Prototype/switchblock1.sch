EESchema Schematic File Version 4
LIBS:Untitled-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB5705E
P 4500 1350
AR Path="/5FB5705E" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB5705E" Ref="S1"  Part="1" 
AR Path="/5FB9A349/5FB5705E" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB5705E" Ref="S6"  Part="1" 
F 0 "S1" V 4900 800 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 450 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 1350 50  0001 C CNN
F 3 "" H 4500 1350 50  0001 C CNN
	1    4500 1350
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 5FB57070
P 3950 850
AR Path="/5FB57070" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB57070" Ref="D1"  Part="1" 
AR Path="/5FB9A349/5FB57070" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB57070" Ref="D6"  Part="1" 
F 0 "D1" V 3850 600 50  0000 L CNN
F 1 "DIODE" V 3950 500 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 850 50  0001 C CNN
F 3 "~" H 3950 850 50  0001 C CNN
	1    3950 850 
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 1100 4300 850 
Wire Wire Line
	4300 850  4150 850 
Wire Wire Line
	3750 850  3650 850 
Wire Wire Line
	3650 850  3650 1100
Text Label 4300 850  0    50   ~ 0
MOSD_1
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB644C5
P 4500 2800
AR Path="/5FB644C5" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB644C5" Ref="S2"  Part="1" 
AR Path="/5FB9A349/5FB644C5" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB644C5" Ref="S7"  Part="1" 
F 0 "S2" V 4900 2250 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 1900 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 5FB644CB
P 3950 2300
AR Path="/5FB644CB" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB644CB" Ref="D2"  Part="1" 
AR Path="/5FB9A349/5FB644CB" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB644CB" Ref="D7"  Part="1" 
F 0 "D2" V 3850 2050 50  0000 L CNN
F 1 "DIODE" V 3950 1950 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 2300 50  0001 C CNN
F 3 "~" H 3950 2300 50  0001 C CNN
	1    3950 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 2550 4300 2300
Wire Wire Line
	4300 2300 4150 2300
Wire Wire Line
	3750 2300 3650 2300
Wire Wire Line
	3650 2300 3650 2550
Text Label 4300 2300 0    50   ~ 0
MOSD_2
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6CC1B
P 4500 4250
AR Path="/5FB6CC1B" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC1B" Ref="S3"  Part="1" 
AR Path="/5FB9A349/5FB6CC1B" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC1B" Ref="S8"  Part="1" 
F 0 "S3" V 4900 3700 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 3350 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 4250 50  0001 C CNN
F 3 "" H 4500 4250 50  0001 C CNN
	1    4500 4250
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 5FB6CC21
P 3950 3750
AR Path="/5FB6CC21" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC21" Ref="D3"  Part="1" 
AR Path="/5FB9A349/5FB6CC21" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC21" Ref="D8"  Part="1" 
F 0 "D3" V 3850 3500 50  0000 L CNN
F 1 "DIODE" V 3950 3400 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 3750 50  0001 C CNN
F 3 "~" H 3950 3750 50  0001 C CNN
	1    3950 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 4000 4300 3750
Wire Wire Line
	4300 3750 4150 3750
Wire Wire Line
	3750 3750 3650 3750
Wire Wire Line
	3650 3750 3650 4000
Text Label 4300 3750 0    50   ~ 0
MOSD_3
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6CC33
P 4500 5700
AR Path="/5FB6CC33" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC33" Ref="S4"  Part="1" 
AR Path="/5FB9A349/5FB6CC33" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC33" Ref="S9"  Part="1" 
F 0 "S4" V 4900 5150 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5000 4800 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4500 5700 50  0001 C CNN
F 3 "" H 4500 5700 50  0001 C CNN
	1    4500 5700
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 5FB6CC39
P 3950 5200
AR Path="/5FB6CC39" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6CC39" Ref="D4"  Part="1" 
AR Path="/5FB9A349/5FB6CC39" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6CC39" Ref="D9"  Part="1" 
F 0 "D4" V 3850 4950 50  0000 L CNN
F 1 "DIODE" V 3950 4850 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 5200 50  0001 C CNN
F 3 "~" H 3950 5200 50  0001 C CNN
	1    3950 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 5450 4300 5200
Wire Wire Line
	4300 5200 4150 5200
Wire Wire Line
	3750 5200 3650 5200
Wire Wire Line
	3650 5200 3650 5450
Text Label 4300 5200 0    50   ~ 0
MOSD_4
$Comp
L switchboard_parts:EE2-5NU-L_EC2-5NU S?
U 1 1 5FB6EB35
P 4550 7150
AR Path="/5FB6EB35" Ref="S?"  Part="1" 
AR Path="/5FB4E0CA/5FB6EB35" Ref="S5"  Part="1" 
AR Path="/5FB9A349/5FB6EB35" Ref="S?"  Part="1" 
AR Path="/5FB9AAE0/5FB6EB35" Ref="S10"  Part="1" 
F 0 "S5" V 4950 6600 50  0000 L CNN
F 1 "EE2-5NU-L_EC2-5NU" V 5050 6250 50  0000 L CNN
F 2 "switchboard_parts:EE2-5NU" H 4550 7150 50  0001 C CNN
F 3 "" H 4550 7150 50  0001 C CNN
	1    4550 7150
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 5FB6EB3B
P 4000 6650
AR Path="/5FB6EB3B" Ref="D?"  Part="1" 
AR Path="/5FB4E0CA/5FB6EB3B" Ref="D5"  Part="1" 
AR Path="/5FB9A349/5FB6EB3B" Ref="D?"  Part="1" 
AR Path="/5FB9AAE0/5FB6EB3B" Ref="D10"  Part="1" 
F 0 "D5" V 3900 6400 50  0000 L CNN
F 1 "DIODE" V 4000 6300 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4000 6650 50  0001 C CNN
F 3 "~" H 4000 6650 50  0001 C CNN
	1    4000 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 6900 4350 6650
Wire Wire Line
	4350 6650 4200 6650
Wire Wire Line
	3800 6650 3700 6650
Wire Wire Line
	3700 6650 3700 6900
Text Label 4350 6650 0    50   ~ 0
MOSD_5
Text HLabel 3650 4300 0    50   BiDi ~ 0
N_NC
Text HLabel 3650 2850 0    50   BiDi ~ 0
N_NC
Text HLabel 3650 1400 0    50   BiDi ~ 0
N_NC
Text HLabel 3650 5750 0    50   BiDi ~ 0
N_NC
Text HLabel 3700 7200 0    50   BiDi ~ 0
N_NC
Text HLabel 3700 7400 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 5950 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 4500 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 3050 0    50   BiDi ~ 0
N_NO
Text HLabel 3650 1600 0    50   BiDi ~ 0
N_NO
Text HLabel 4300 1400 2    50   BiDi ~ 0
P_NC
Text HLabel 4300 1600 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 2850 2    50   BiDi ~ 0
P_NC
Text HLabel 4300 4300 2    50   BiDi ~ 0
P_NC
Text HLabel 4300 5750 2    50   BiDi ~ 0
P_NC
Text HLabel 4350 7200 2    50   BiDi ~ 0
P_NC
Text HLabel 4300 3050 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 4500 2    50   BiDi ~ 0
P_NO
Text HLabel 4300 5950 2    50   BiDi ~ 0
P_NO
Text HLabel 4350 7400 2    50   BiDi ~ 0
P_NO
Text HLabel 3650 850  0    50   Input ~ 0
5V_IN
Text HLabel 3650 3750 0    50   Input ~ 0
5V_IN
Text HLabel 3650 5200 0    50   Input ~ 0
5V_IN
Text HLabel 3700 6650 0    50   Input ~ 0
5V_IN
Text HLabel 3650 2300 0    50   Input ~ 0
5V_IN
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8FECA
P 6050 1150
AR Path="/5FB8FECA" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FECA" Ref="M1"  Part="1" 
AR Path="/5FB9A349/5FB8FECA" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FECA" Ref="M6"  Part="1" 
F 0 "M1" H 6208 946 50  0000 L CNN
F 1 "NTD3055L170" H 6208 855 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 1150 50  0001 C CNN
F 3 "" H 6050 1150 50  0001 C CNN
	1    6050 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8FED0
P 5700 1400
AR Path="/5FB8FED0" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FED0" Ref="R1"  Part="1" 
AR Path="/5FB9A349/5FB8FED0" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FED0" Ref="R11"  Part="1" 
F 0 "R1" V 5493 1400 50  0000 C CNN
F 1 "1.2k" V 5584 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 1400 50  0001 C CNN
F 3 "~" H 5700 1400 50  0001 C CNN
	1    5700 1400
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8FED6
P 5850 1550
AR Path="/5FB8FED6" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8FED6" Ref="R6"  Part="1" 
AR Path="/5FB9A349/5FB8FED6" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8FED6" Ref="R16"  Part="1" 
F 0 "R6" H 5650 1600 50  0000 L CNN
F 1 "43k" H 5650 1500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 1550 50  0001 C CNN
F 3 "~" H 5850 1550 50  0001 C CNN
	1    5850 1550
	1    0    0    -1  
$EndComp
Text Label 6150 1200 0    50   ~ 0
MOSD_1
Connection ~ 5850 1400
Text HLabel 6150 1600 3    50   Input ~ 0
GND_IN
Text HLabel 5850 1700 3    50   Input ~ 0
GND_IN
Text HLabel 5550 1400 0    50   BiDi ~ 0
CE_1
Text HLabel 5550 7000 0    50   BiDi ~ 0
CE_5
Text HLabel 5550 5600 0    50   BiDi ~ 0
CE_4
Text HLabel 5550 4200 0    50   BiDi ~ 0
CE_3
Text HLabel 5550 2800 0    50   BiDi ~ 0
CE_2
Text HLabel 6150 7200 3    50   Input ~ 0
GND_IN
Text HLabel 5850 7300 3    50   Input ~ 0
GND_IN
Text HLabel 5850 5900 3    50   Input ~ 0
GND_IN
Text HLabel 6150 5800 3    50   Input ~ 0
GND_IN
Text HLabel 6150 4400 3    50   Input ~ 0
GND_IN
Text HLabel 5850 4500 3    50   Input ~ 0
GND_IN
Text HLabel 5850 3100 3    50   Input ~ 0
GND_IN
Text HLabel 6150 3000 3    50   Input ~ 0
GND_IN
Text Label 6150 2600 0    50   ~ 0
MOSD_2
$Comp
L Device:R R?
U 1 1 5FB8E50C
P 5850 2950
AR Path="/5FB8E50C" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E50C" Ref="R7"  Part="1" 
AR Path="/5FB9A349/5FB8E50C" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E50C" Ref="R17"  Part="1" 
F 0 "R7" H 5650 3000 50  0000 L CNN
F 1 "43k" H 5650 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 2950 50  0001 C CNN
F 3 "~" H 5850 2950 50  0001 C CNN
	1    5850 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8E506
P 5700 2800
AR Path="/5FB8E506" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E506" Ref="R2"  Part="1" 
AR Path="/5FB9A349/5FB8E506" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E506" Ref="R12"  Part="1" 
F 0 "R2" V 5493 2800 50  0000 C CNN
F 1 "1.2k" V 5584 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 2800 50  0001 C CNN
F 3 "~" H 5700 2800 50  0001 C CNN
	1    5700 2800
	0    1    1    0   
$EndComp
Connection ~ 5850 2800
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8E500
P 6050 2550
AR Path="/5FB8E500" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8E500" Ref="M2"  Part="1" 
AR Path="/5FB9A349/5FB8E500" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8E500" Ref="M7"  Part="1" 
F 0 "M2" H 6208 2346 50  0000 L CNN
F 1 "NTD3055L170" H 6208 2255 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 2550 50  0001 C CNN
F 3 "" H 6050 2550 50  0001 C CNN
	1    6050 2550
	1    0    0    -1  
$EndComp
Text Label 6150 4000 0    50   ~ 0
MOSD_3
$Comp
L Device:R R?
U 1 1 5FB8C70A
P 5850 4350
AR Path="/5FB8C70A" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C70A" Ref="R8"  Part="1" 
AR Path="/5FB9A349/5FB8C70A" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C70A" Ref="R18"  Part="1" 
F 0 "R8" H 5650 4400 50  0000 L CNN
F 1 "43k" H 5650 4300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 4350 50  0001 C CNN
F 3 "~" H 5850 4350 50  0001 C CNN
	1    5850 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB8C704
P 5700 4200
AR Path="/5FB8C704" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C704" Ref="R3"  Part="1" 
AR Path="/5FB9A349/5FB8C704" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C704" Ref="R13"  Part="1" 
F 0 "R3" V 5493 4200 50  0000 C CNN
F 1 "1.2k" V 5584 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 4200 50  0001 C CNN
F 3 "~" H 5700 4200 50  0001 C CNN
	1    5700 4200
	0    1    1    0   
$EndComp
Connection ~ 5850 4200
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8C6FE
P 6050 3950
AR Path="/5FB8C6FE" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8C6FE" Ref="M3"  Part="1" 
AR Path="/5FB9A349/5FB8C6FE" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8C6FE" Ref="M8"  Part="1" 
F 0 "M3" H 6208 3746 50  0000 L CNN
F 1 "NTD3055L170" H 6208 3655 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 3950 50  0001 C CNN
F 3 "" H 6050 3950 50  0001 C CNN
	1    6050 3950
	1    0    0    -1  
$EndComp
Text Label 6150 5400 0    50   ~ 0
MOSD_4
$Comp
L Device:R R?
U 1 1 5FB88A40
P 5850 5750
AR Path="/5FB88A40" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A40" Ref="R9"  Part="1" 
AR Path="/5FB9A349/5FB88A40" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A40" Ref="R19"  Part="1" 
F 0 "R9" H 5650 5800 50  0000 L CNN
F 1 "43k" H 5650 5700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 5750 50  0001 C CNN
F 3 "~" H 5850 5750 50  0001 C CNN
	1    5850 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB88A3A
P 5700 5600
AR Path="/5FB88A3A" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A3A" Ref="R4"  Part="1" 
AR Path="/5FB9A349/5FB88A3A" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A3A" Ref="R14"  Part="1" 
F 0 "R4" V 5493 5600 50  0000 C CNN
F 1 "1.2k" V 5584 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 5600 50  0001 C CNN
F 3 "~" H 5700 5600 50  0001 C CNN
	1    5700 5600
	0    1    1    0   
$EndComp
Connection ~ 5850 5600
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB88A34
P 6050 5350
AR Path="/5FB88A34" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB88A34" Ref="M4"  Part="1" 
AR Path="/5FB9A349/5FB88A34" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB88A34" Ref="M9"  Part="1" 
F 0 "M4" H 6208 5146 50  0000 L CNN
F 1 "NTD3055L170" H 6208 5055 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 5350 50  0001 C CNN
F 3 "" H 6050 5350 50  0001 C CNN
	1    6050 5350
	1    0    0    -1  
$EndComp
Text Label 6150 6800 0    50   ~ 0
MOSD_5
$Comp
L Device:R R?
U 1 1 5FB84198
P 5850 7150
AR Path="/5FB84198" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB84198" Ref="R10"  Part="1" 
AR Path="/5FB9A349/5FB84198" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB84198" Ref="R20"  Part="1" 
F 0 "R10" H 5650 7200 50  0000 L CNN
F 1 "43k" H 5650 7100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 7150 50  0001 C CNN
F 3 "~" H 5850 7150 50  0001 C CNN
	1    5850 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FB84192
P 5700 7000
AR Path="/5FB84192" Ref="R?"  Part="1" 
AR Path="/5FB4E0CA/5FB84192" Ref="R5"  Part="1" 
AR Path="/5FB9A349/5FB84192" Ref="R?"  Part="1" 
AR Path="/5FB9AAE0/5FB84192" Ref="R15"  Part="1" 
F 0 "R5" V 5493 7000 50  0000 C CNN
F 1 "1.2k" V 5584 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5630 7000 50  0001 C CNN
F 3 "~" H 5700 7000 50  0001 C CNN
	1    5700 7000
	0    1    1    0   
$EndComp
Connection ~ 5850 7000
$Comp
L switchboard_parts:NTD3055L170 M?
U 1 1 5FB8418C
P 6050 6750
AR Path="/5FB8418C" Ref="M?"  Part="1" 
AR Path="/5FB4E0CA/5FB8418C" Ref="M5"  Part="1" 
AR Path="/5FB9A349/5FB8418C" Ref="M?"  Part="1" 
AR Path="/5FB9AAE0/5FB8418C" Ref="M10"  Part="1" 
F 0 "M5" H 6208 6546 50  0000 L CNN
F 1 "NTD3055L170" H 6208 6455 50  0000 L CNN
F 2 "switchboard_parts:NTD3055-DPAK" H 6050 6750 50  0001 C CNN
F 3 "" H 6050 6750 50  0001 C CNN
	1    6050 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D11
U 1 1 5FC1F2E0
P 8850 1400
AR Path="/5FB4E0CA/5FC1F2E0" Ref="D11"  Part="1" 
AR Path="/5FB9AAE0/5FC1F2E0" Ref="D16"  Part="1" 
F 0 "D11" H 8843 1145 50  0000 C CNN
F 1 "LED" H 8843 1236 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 1400 50  0001 C CNN
F 3 "~" H 8850 1400 50  0001 C CNN
	1    8850 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D12
U 1 1 5FC21A14
P 8850 2000
AR Path="/5FB4E0CA/5FC21A14" Ref="D12"  Part="1" 
AR Path="/5FB9AAE0/5FC21A14" Ref="D17"  Part="1" 
F 0 "D12" H 8843 1745 50  0000 C CNN
F 1 "LED" H 8843 1836 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 2000 50  0001 C CNN
F 3 "~" H 8850 2000 50  0001 C CNN
	1    8850 2000
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D13
U 1 1 5FC245D3
P 8850 2600
AR Path="/5FB4E0CA/5FC245D3" Ref="D13"  Part="1" 
AR Path="/5FB9AAE0/5FC245D3" Ref="D18"  Part="1" 
F 0 "D13" H 8843 2345 50  0000 C CNN
F 1 "LED" H 8843 2436 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 2600 50  0001 C CNN
F 3 "~" H 8850 2600 50  0001 C CNN
	1    8850 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D14
U 1 1 5FC24F3A
P 8850 3200
AR Path="/5FB4E0CA/5FC24F3A" Ref="D14"  Part="1" 
AR Path="/5FB9AAE0/5FC24F3A" Ref="D19"  Part="1" 
F 0 "D14" H 8843 2945 50  0000 C CNN
F 1 "LED" H 8843 3036 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 3200 50  0001 C CNN
F 3 "~" H 8850 3200 50  0001 C CNN
	1    8850 3200
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D15
U 1 1 5FC25A6C
P 8850 3850
AR Path="/5FB4E0CA/5FC25A6C" Ref="D15"  Part="1" 
AR Path="/5FB9AAE0/5FC25A6C" Ref="D20"  Part="1" 
F 0 "D15" H 8843 3595 50  0000 C CNN
F 1 "LED" H 8843 3686 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8850 3850 50  0001 C CNN
F 3 "~" H 8850 3850 50  0001 C CNN
	1    8850 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R21
U 1 1 5FC2719C
P 8550 1400
AR Path="/5FB4E0CA/5FC2719C" Ref="R21"  Part="1" 
AR Path="/5FB9AAE0/5FC2719C" Ref="R26"  Part="1" 
F 0 "R21" V 8343 1400 50  0000 C CNN
F 1 "150" V 8434 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 1400 50  0001 C CNN
F 3 "~" H 8550 1400 50  0001 C CNN
	1    8550 1400
	0    1    1    0   
$EndComp
$Comp
L Device:R R22
U 1 1 5FC27FE8
P 8550 2000
AR Path="/5FB4E0CA/5FC27FE8" Ref="R22"  Part="1" 
AR Path="/5FB9AAE0/5FC27FE8" Ref="R27"  Part="1" 
F 0 "R22" V 8343 2000 50  0000 C CNN
F 1 "150" V 8434 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 2000 50  0001 C CNN
F 3 "~" H 8550 2000 50  0001 C CNN
	1    8550 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R23
U 1 1 5FC285A6
P 8550 2600
AR Path="/5FB4E0CA/5FC285A6" Ref="R23"  Part="1" 
AR Path="/5FB9AAE0/5FC285A6" Ref="R28"  Part="1" 
F 0 "R23" V 8343 2600 50  0000 C CNN
F 1 "150" V 8434 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 2600 50  0001 C CNN
F 3 "~" H 8550 2600 50  0001 C CNN
	1    8550 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 5FC28FD1
P 8550 3200
AR Path="/5FB4E0CA/5FC28FD1" Ref="R24"  Part="1" 
AR Path="/5FB9AAE0/5FC28FD1" Ref="R29"  Part="1" 
F 0 "R24" V 8343 3200 50  0000 C CNN
F 1 "150" V 8434 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3200 50  0001 C CNN
F 3 "~" H 8550 3200 50  0001 C CNN
	1    8550 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5FC29607
P 8550 3850
AR Path="/5FB4E0CA/5FC29607" Ref="R25"  Part="1" 
AR Path="/5FB9AAE0/5FC29607" Ref="R30"  Part="1" 
F 0 "R25" V 8343 3850 50  0000 C CNN
F 1 "150" V 8434 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3850 50  0001 C CNN
F 3 "~" H 8550 3850 50  0001 C CNN
	1    8550 3850
	0    1    1    0   
$EndComp
Text Label 8400 1400 2    50   ~ 0
5V_IN
Text Label 8400 2000 2    50   ~ 0
5V_IN
Text Label 8400 2600 2    50   ~ 0
5V_IN
Text Label 8400 3200 2    50   ~ 0
5V_IN
Text Label 8400 3850 2    50   ~ 0
5V_IN
Text Label 9000 1400 0    50   ~ 0
MOSD_1
Text Label 9000 2000 0    50   ~ 0
MOSD_2
Text Label 9000 2600 0    50   ~ 0
MOSD_3
Text Label 9000 3200 0    50   ~ 0
MOSD_4
Text Label 9000 3850 0    50   ~ 0
MOSD_5
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 5FC41AE1
P 8750 4500
AR Path="/5FB9AAE0/5FC41AE1" Ref="J15"  Part="1" 
AR Path="/5FB4E0CA/5FC41AE1" Ref="J10"  Part="1" 
F 0 "J10" H 8830 4492 50  0000 L CNN
F 1 "Conn_01x02" H 8830 4401 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 4500 50  0001 C CNN
F 3 "~" H 8750 4500 50  0001 C CNN
	1    8750 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 5FC42918
P 8750 4850
AR Path="/5FB9AAE0/5FC42918" Ref="J16"  Part="1" 
AR Path="/5FB4E0CA/5FC42918" Ref="J11"  Part="1" 
F 0 "J11" H 8830 4842 50  0000 L CNN
F 1 "Conn_01x02" H 8830 4751 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 4850 50  0001 C CNN
F 3 "~" H 8750 4850 50  0001 C CNN
	1    8750 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 5FC43074
P 8750 5200
AR Path="/5FB9AAE0/5FC43074" Ref="J17"  Part="1" 
AR Path="/5FB4E0CA/5FC43074" Ref="J12"  Part="1" 
F 0 "J12" H 8830 5192 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5101 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5200 50  0001 C CNN
F 3 "~" H 8750 5200 50  0001 C CNN
	1    8750 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J18
U 1 1 5FC43479
P 8750 5550
AR Path="/5FB9AAE0/5FC43479" Ref="J18"  Part="1" 
AR Path="/5FB4E0CA/5FC43479" Ref="J13"  Part="1" 
F 0 "J13" H 8830 5542 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5451 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5550 50  0001 C CNN
F 3 "~" H 8750 5550 50  0001 C CNN
	1    8750 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J19
U 1 1 5FC43A22
P 8750 5900
AR Path="/5FB9AAE0/5FC43A22" Ref="J19"  Part="1" 
AR Path="/5FB4E0CA/5FC43A22" Ref="J14"  Part="1" 
F 0 "J14" H 8830 5892 50  0000 L CNN
F 1 "Conn_01x02" H 8830 5801 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 8750 5900 50  0001 C CNN
F 3 "~" H 8750 5900 50  0001 C CNN
	1    8750 5900
	1    0    0    -1  
$EndComp
Text Label 4300 1500 0    50   ~ 0
PPIV1
Text Label 3650 1500 2    50   ~ 0
NPIV1
Text Label 4300 2950 0    50   ~ 0
PPIV2
Text Label 4300 4400 0    50   ~ 0
PPIV3
Text Label 4300 5850 0    50   ~ 0
PPIV4
Text Label 4350 7300 0    50   ~ 0
PPIV5
Text Label 3650 2950 2    50   ~ 0
NPIV2
Text Label 3650 4400 2    50   ~ 0
NPIV3
Text Label 3650 5850 2    50   ~ 0
NPIV4
Text Label 3700 7300 2    50   ~ 0
NPIV5
Text Label 8550 4500 2    50   ~ 0
PPIV1
Text Label 8550 4600 2    50   ~ 0
NPIV1
Text Label 8550 4850 2    50   ~ 0
PPIV2
Text Label 8550 4950 2    50   ~ 0
NPIV2
Text Label 8550 5200 2    50   ~ 0
PPIV3
Text Label 8550 5300 2    50   ~ 0
NPIV3
Text Label 8550 5550 2    50   ~ 0
PPIV4
Text Label 8550 5650 2    50   ~ 0
NPIV4
Text Label 8550 5900 2    50   ~ 0
PPIV5
Text Label 8550 6000 2    50   ~ 0
NPIV5
$EndSCHEMATC
