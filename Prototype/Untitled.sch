EESchema Schematic File Version 4
LIBS:Untitled-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2750 950  0    50   ~ 0
5VIN
Text Label 2450 1250 2    50   ~ 0
DUE_GND
Text Label 2100 950  2    50   ~ 0
DUE_5V
Wire Wire Line
	2450 950  2100 950 
Wire Wire Line
	2750 950  2450 950 
Connection ~ 2450 950 
$Comp
L Device:CP C1
U 1 1 5F9C9058
P 2450 1100
F 0 "C1" H 2568 1146 50  0000 L CNN
F 1 "10u" H 2568 1055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 2488 950 50  0001 C CNN
F 3 "~" H 2450 1100 50  0001 C CNN
	1    2450 1100
	1    0    0    -1  
$EndComp
$Sheet
S 3650 850  800  1850
U 5FB4E0CA
F0 "Switchblock 1" 50
F1 "switchblock1.sch" 50
F2 "5V_IN" I L 3650 950 50 
F3 "GND_IN" I L 3650 1050 50 
F4 "N_NC" B L 3650 1350 50 
F5 "N_NO" B L 3650 1650 50 
F6 "P_NC" B L 3650 1250 50 
F7 "P_NO" B L 3650 1550 50 
F8 "CE_1" B L 3650 2200 50 
F9 "CE_5" B L 3650 2600 50 
F10 "CE_4" B L 3650 2500 50 
F11 "CE_3" B L 3650 2400 50 
F12 "CE_2" B L 3650 2300 50 
$EndSheet
$Sheet
S 3650 3050 800  1850
U 5FB9AAE0
F0 "Switchblock 2" 50
F1 "switchblock1.sch" 50
F2 "N_NC" B L 3650 3550 50 
F3 "N_NO" B L 3650 3850 50 
F4 "P_NC" B L 3650 3450 50 
F5 "P_NO" B L 3650 3750 50 
F6 "5V_IN" I L 3650 3150 50 
F7 "GND_IN" I L 3650 3250 50 
F8 "CE_1" B L 3650 4400 50 
F9 "CE_5" B L 3650 4800 50 
F10 "CE_4" B L 3650 4700 50 
F11 "CE_3" B L 3650 4600 50 
F12 "CE_2" B L 3650 4500 50 
$EndSheet
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 5FBA89B2
P 1400 2350
F 0 "J1" H 1480 2342 50  0000 L CNN
F 1 "Conn_01x10" H 1480 2251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 1400 2350 50  0001 C CNN
F 3 "~" H 1400 2350 50  0001 C CNN
	1    1400 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5FBAFF56
P 1400 3550
F 0 "J2" H 1318 2925 50  0000 C CNN
F 1 "Conn_01x08" H 1318 3016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 1400 3550 50  0001 C CNN
F 3 "~" H 1400 3550 50  0001 C CNN
	1    1400 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5FBB10F6
P 1400 4700
F 0 "J3" H 1318 4075 50  0000 C CNN
F 1 "Conn_01x08" H 1318 4166 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 1400 4700 50  0001 C CNN
F 3 "~" H 1400 4700 50  0001 C CNN
	1    1400 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x18_Odd_Even J4
U 1 1 5FBB278C
P 1700 6500
F 0 "J4" H 1750 7517 50  0000 C CNN
F 1 "Conn_02x18_Odd_Even" H 1750 7426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x18_P2.54mm_Vertical" H 1700 6500 50  0001 C CNN
F 3 "~" H 1700 6500 50  0001 C CNN
	1    1700 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J5
U 1 1 5FBBB644
P 2550 2350
F 0 "J5" H 2468 1725 50  0000 C CNN
F 1 "Conn_01x08" H 2468 1816 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2550 2350 50  0001 C CNN
F 3 "~" H 2550 2350 50  0001 C CNN
	1    2550 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J6
U 1 1 5FBBFFF9
P 2550 3550
F 0 "J6" H 2468 2925 50  0000 C CNN
F 1 "Conn_01x08" H 2468 3016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2550 3550 50  0001 C CNN
F 3 "~" H 2550 3550 50  0001 C CNN
	1    2550 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J7
U 1 1 5FBC134B
P 2550 4700
F 0 "J7" H 2468 4075 50  0000 C CNN
F 1 "Conn_01x08" H 2468 4166 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2550 4700 50  0001 C CNN
F 3 "~" H 2550 4700 50  0001 C CNN
	1    2550 4700
	1    0    0    -1  
$EndComp
Text Label 1200 1950 2    50   ~ 0
DUE_SCL
Text Label 1200 2050 2    50   ~ 0
DUE_SDA
Text Label 1200 2150 2    50   ~ 0
DUE_AREF
Text Label 1200 2250 2    50   ~ 0
DUE_GND
Text Label 1200 2350 2    50   ~ 0
DUE_D13
Text Label 1200 2450 2    50   ~ 0
DUE_D12
Text Label 1200 2550 2    50   ~ 0
DUE_D11
Text Label 1200 2650 2    50   ~ 0
DUE_D10
Text Label 1200 2750 2    50   ~ 0
DUE_D9
Text Label 1200 2850 2    50   ~ 0
DUE_D8
Text Label 1200 3250 2    50   ~ 0
DUE_D7
Text Label 1200 3350 2    50   ~ 0
DUE_D6
Text Label 1200 3450 2    50   ~ 0
DUE_D5
Text Label 1200 3550 2    50   ~ 0
DUE_D4
Text Label 1200 3650 2    50   ~ 0
DUE_D3
Text Label 1200 3750 2    50   ~ 0
DUE_D2
Text Label 1200 3850 2    50   ~ 0
DUE_TX0
Text Label 1200 3950 2    50   ~ 0
DUE_RX0
Text Label 1200 4400 2    50   ~ 0
DUE_D14
Text Label 1200 4500 2    50   ~ 0
DUE_D15
Text Label 1200 4600 2    50   ~ 0
DUE_D16
Text Label 1200 4700 2    50   ~ 0
DUE_D17
Text Label 1200 4800 2    50   ~ 0
DUE_D18
Text Label 1200 4900 2    50   ~ 0
DUE_D19
Text Label 1200 5000 2    50   ~ 0
DUE_D20
Text Label 1200 5100 2    50   ~ 0
DUE_D21
Text Label 1500 5700 2    50   ~ 0
DUE_5V
Text Label 1500 5800 2    50   ~ 0
DUE_D22
Text Label 1500 5900 2    50   ~ 0
DUE_D24
Text Label 1500 6000 2    50   ~ 0
DUE_D26
Text Label 1500 6100 2    50   ~ 0
DUE_D28
Text Label 1500 6200 2    50   ~ 0
DUE_D30
Text Label 1500 6300 2    50   ~ 0
DUE_D32
Text Label 1500 6400 2    50   ~ 0
DUE_D34
Text Label 1500 6500 2    50   ~ 0
DUE_D36
Text Label 1500 6600 2    50   ~ 0
DUE_D38
Text Label 1500 6700 2    50   ~ 0
DUE_D40
Text Label 1500 6800 2    50   ~ 0
DUE_D42
Text Label 1500 6900 2    50   ~ 0
DUE_D44
Text Label 1500 7000 2    50   ~ 0
DUE_D46
Text Label 1500 7100 2    50   ~ 0
DUE_D48
Text Label 1500 7200 2    50   ~ 0
DUE_D50
Text Label 1500 7300 2    50   ~ 0
DUE_D52
Text Label 1500 7400 2    50   ~ 0
DUE_GND
Text Label 2000 5700 0    50   ~ 0
DUE_5V
Text Label 2150 5800 0    50   ~ 0
DUE_D23
Text Label 2000 5900 0    50   ~ 0
DUE_D25
Text Label 2000 6000 0    50   ~ 0
DUE_D27
Text Label 2000 6100 0    50   ~ 0
DUE_D29
Text Label 2000 6200 0    50   ~ 0
DUE_D31
Text Label 2000 6300 0    50   ~ 0
DUE_D33
Text Label 2000 6400 0    50   ~ 0
DUE_D35
Text Label 2000 6500 0    50   ~ 0
DUE_D37
Text Label 2000 6600 0    50   ~ 0
DUE_D39
Text Label 2000 6700 0    50   ~ 0
DUE_D41
Text Label 2000 6800 0    50   ~ 0
DUE_D43
Text Label 2000 6900 0    50   ~ 0
DUE_D45
Text Label 2000 7000 0    50   ~ 0
DUE_D47
Text Label 2000 7100 0    50   ~ 0
DUE_D49
Text Label 2000 7200 0    50   ~ 0
DUE_D51
Text Label 2000 7300 0    50   ~ 0
DUE_D53
Text Label 2000 7400 0    50   ~ 0
DUE_GND
Text Label 2350 2050 2    50   ~ 0
DUE_A8
Text Label 2350 2150 2    50   ~ 0
DUE_A9
Text Label 2350 2250 2    50   ~ 0
DUE_A10
Text Label 2350 2350 2    50   ~ 0
DUE_A11
Text Label 2350 2450 2    50   ~ 0
DUE_DAC2
Text Label 2350 2550 2    50   ~ 0
DUE_DAC1
Text Label 2350 2650 2    50   ~ 0
DUE_CANRX
Text Label 2350 2750 2    50   ~ 0
DUE_CANTX
Text Label 2350 3250 2    50   ~ 0
DUE_A0
Text Label 2350 3350 2    50   ~ 0
DUE_A1
Text Label 2350 3450 2    50   ~ 0
DUE_A2
Text Label 2350 3550 2    50   ~ 0
DUE_A3
Text Label 2350 3650 2    50   ~ 0
DUE_A4
Text Label 2350 3750 2    50   ~ 0
DUE_A5
Text Label 2350 3850 2    50   ~ 0
DUE_A6
Text Label 2350 3950 2    50   ~ 0
DUE_A7
Text Label 2350 4400 2    50   ~ 0
DUE_NC
Text Label 2350 4500 2    50   ~ 0
DUE_IOREF
Text Label 2350 4600 2    50   ~ 0
DUE_RESET
Text Label 2350 4700 2    50   ~ 0
DUE_3V3
Text Label 2350 4800 2    50   ~ 0
DUE_5V
Text Label 2350 4900 2    50   ~ 0
DUE_GND
Text Label 2350 5000 2    50   ~ 0
DUE_GND
Text Label 2350 5100 2    50   ~ 0
DUE_VIN
Text Label 3650 950  2    50   ~ 0
5VIN
Text Label 3650 1050 2    50   ~ 0
DUE_GND
Text Label 3650 3150 2    50   ~ 0
5VIN
Text Label 3650 3250 2    50   ~ 0
DUE_GND
Text Label 3650 2200 2    50   ~ 0
DUE_D13
Text Label 3650 2300 2    50   ~ 0
DUE_D12
Text Label 3650 2400 2    50   ~ 0
DUE_D10
Text Label 3650 2500 2    50   ~ 0
DUE_D9
Text Label 3650 2600 2    50   ~ 0
DUE_D4
Text Label 3650 4400 2    50   ~ 0
DUE_D14
Text Label 3650 4500 2    50   ~ 0
DUE_D15
Text Label 3650 4600 2    50   ~ 0
DUE_D20
Text Label 3650 4700 2    50   ~ 0
DUE_D46
Text Label 3500 4800 2    50   ~ 0
DUE_D23
$Comp
L power:+5V #PWR0101
U 1 1 5FC09CC3
P 2100 950
F 0 "#PWR0101" H 2100 800 50  0001 C CNN
F 1 "+5V" H 2115 1123 50  0000 C CNN
F 2 "" H 2100 950 50  0001 C CNN
F 3 "" H 2100 950 50  0001 C CNN
	1    2100 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FC0AA00
P 2450 1250
F 0 "#PWR0102" H 2450 1000 50  0001 C CNN
F 1 "GND" H 2455 1077 50  0000 C CNN
F 2 "" H 2450 1250 50  0001 C CNN
F 3 "" H 2450 1250 50  0001 C CNN
	1    2450 1250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5FC4EC7F
P 1100 800
F 0 "J8" H 1180 792 50  0000 L CNN
F 1 "Conn_01x02" H 1180 701 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 1100 800 50  0001 C CNN
F 3 "~" H 1100 800 50  0001 C CNN
	1    1100 800 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5FC50509
P 1100 1150
F 0 "J9" H 1180 1142 50  0000 L CNN
F 1 "Conn_01x02" H 1180 1051 50  0000 L CNN
F 2 "switchboard_parts:Connector_01x02_solder" H 1100 1150 50  0001 C CNN
F 3 "~" H 1100 1150 50  0001 C CNN
	1    1100 1150
	1    0    0    -1  
$EndComp
Text Label 900  800  2    50   ~ 0
PNC
Text Label 900  900  2    50   ~ 0
NNC
Text Label 900  1150 2    50   ~ 0
PNO
Text Label 900  1250 2    50   ~ 0
NNO
Text Label 3650 1250 2    50   ~ 0
PNC
Text Label 3650 1350 2    50   ~ 0
NNC
Text Label 3650 1550 2    50   ~ 0
PNO
Text Label 3650 1650 2    50   ~ 0
NNO
Text Label 3650 3450 2    50   ~ 0
PNC
Text Label 3650 3550 2    50   ~ 0
NNC
Text Label 3650 3750 2    50   ~ 0
PNO
Text Label 3650 3850 2    50   ~ 0
NNO
Wire Wire Line
	3650 4800 3500 4800
Wire Wire Line
	2000 5800 2150 5800
$EndSCHEMATC
